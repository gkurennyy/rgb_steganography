import dictionary as dict
import color_dictionary as color_dict
import displaySimulate

colors = ['Re', 'Or', 'Ye', 'Gr', 'Lb', 'Bl', 'Pu', 'Pi']
Codes = ['000', '011', '110', '001', '100', '010', '101', '111']

colors_cods = {colors[i]: Codes[i] for i in range(len(colors))}
colors_by_cods = {Codes[i]: colors[i] for i in range(len(colors))}

nX, nY = 8, 8

colorDict = color_dict.corolDictionary()

white_diods = colorDict.get_coords_white(nX, nY)

diod_len = nX * nY - len(white_diods)

Dict = dict.dictionary1()


def Hemming_value(arr, k):
	summ = 0
	# print(str(k), end=': ')
	for i in range(k, len(arr) + 1, 2 * k):

		for j in range(i, i + k):
			if j <= len(arr) and j != k - 1:
				summ += arr[j - 1]
	#            print(j, end=' ')
	# print()
	return summ % 2


def Hemming_codding(arr):
	arrOut = list(i for i in arr)
	k = 1
	while k < len(arrOut):
		arrOut.insert(k - 1, 0)
		k *= 2

	k = 1
	while k < len(arrOut):
		arrOut[k - 1] = Hemming_value(arrOut, k)  # summ % 2
		k *= 2
	return arrOut


def Hemming_decode(arr):
	arrNew = list(i for i in arr)
	k = 1
	while k < len(arrNew):
		arrNew[k - 1] = 0
		arrNew[k - 1] = Hemming_value(arrNew, k)
		k *= 2
	k = 1
	check_error = -1
	while k < len(arrNew):
		check_error += k if arrNew[k - 1] != arr[k - 1] else 0
		k *= 2
	# print(*arrNew)
	# print(check_error)
	if check_error >= 0 and check_error < len(arr):
		arrNew[check_error] += 1
		arrNew[check_error] %= 2
	# print(*arrNew)
	k //= 2
	while k > 0:
		arrNew.pop(k - 1)
		k //= 2
	return arrNew


def code_to_color(S):
	out = []
	for i in range(len(S)):
		k = S.pop(0)
		out.append(colors_by_cods[k[0:3]])
		out.append(colors_by_cods[k[3:]])
	return out


def print_colors(S):
	k = 0
	while k + diod_len < len(S):
		print(*S[k:k + diod_len])
		k += diod_len
	print(*S[k:])

def list_to_formatList(List):
	Dict = colorDict.format_arr()



	ListNew = [0]*len(List)


def list_to_string(List):
	global nX, nY, white_diods, colors

	arr = list(['BL']*nX for i in range(nY))

	for i in arr:
		print(i)

	for i in white_diods:
		arr[i[0]][i[1]] = 'Wh'

	arrForm = colorDict.format_arr()
	if len(List) > len(arrForm):
		List = List[:len(arrForm)]

	for i in range(len(List)):
		a = arrForm[i]

		arr[a//8][a%8] = List[i]

	#count = 0
#	for i in range(nX*nY):
#		if [i//nY, i%nY] in white_diods:
#			arr[-1].append('Wh')
#		else:
#			if i-count < len(List):
#				arr[-1].append(List[i-count])
#			else:
#				arr[-1].append('BL')
#
#		if i - nX*nY == 1:
#			break
#		if i % (nY-1) == 0:
#			arr.append([])
#
	str_out = ''
	for i in arr:
		for j in i:
			str_out += j
			str_out += ' '

	str_out = str_out[:-1]
	print(str_out)
	return str_out

def to_format(List):
	A = [0, 1, 0, 3, 4, 0, 6, 7,
		 8, 0, 0, 0, 0, 0, 0, 15,
		 0, 0, 0, 0, 0, 0, 0, 0,
		 24, 0, 0, 27, 28, 0, 0, 0,
		 32, 0, 0, 35, 36, 0, 0, 0,
		 0, 0, 0, 0, 0, 0, 0, 0,
		 48, 0, 0, 0, 0, 0, 0, 0,
		 56, 0, 0, 0, 0, 0, 0, 63, ]

S1 = input()
print(Dict.get_code_str(S1))
S = code_to_color(Dict.get_code_str(S1))
print_colors(S)
S1 = list_to_string(S)
displaySimulate.main(S1)
