class dictionary1(object):
    def __init__(self):
        self.to_upper = 'uP//'  # Следать прописным след символ
        self.to_lower = 'uD//'  # Сделать строчным  след симвов
        self.to_upper_global = 'uPP//'  # Следать прописными все след символы до uDD
        self.to_lower_global = 'uDD//'  # Сделать строчными  все след символы до uPP

        self.symbols_codes = {
            'a': '000000',
            'b': '000001',
            'c': '000010',
            'd': '000011',
            'e': '000100',
            'f': '000101',
            'g': '000110',
            'h': '000111',
            'i': '001000',
            'j': '001001',
            'k': '001010',
            'l': '001011',
            'm': '001100',
            'n': '001101',
            'o': '001110',
            'p': '001111',
            'q': '010000',
            'r': '010001',
            's': '010010',
            't': '010011',
            'u': '010100',
            'v': '010101',
            'w': '010110',
            'x': '010111',
            'y': '011000',
            'z': '011001',

            '&': '011010',
            '%': '011011',
            '=': '011100',
            '#': '011101',
            '$': '011110',
            '*': '011111',
            '(': '100000',
            ')': '100001',

            self.to_upper:        '100010',
            self.to_lower:        '100011',
            self.to_upper_global: '100100',
            self.to_lower_global: '100101',

            'http://':  '100110',
            'https://': '100111',
            '.html':    '101000',

            '!': '101001',
            '>': '101010',
            '<': '101011',
            '?': '101100',
            '_': '101101',
            '+': '101110',
            '-': '101111',

            '0': '110000',
            '1': '110001',
            '2': '110010',
            '3': '110011',
            '4': '110100',
            '5': '110101',
            '6': '110110',
            '7': '110111',
            '8': '111000',
            '9': '111001',

            '/':  '111010',
            '\\': '111011',
            '.':  '111100',
            ',':  '111101',
            ':':  '111110',
            ';':  '111111'
        }

        self.symbols_by_codes = {v: k for k, v in self.symbols_codes.items()}

    def get_sumbol_by_code(self, a):
        return self.symbols_by_codes.get(a)

    def get_code_by_symbol(self, a):
        return self.symbols_codes.get(a)

    def search_phrases_in_dir(self):
        phrases = []
        for i in self.symbols_codes.keys():
            if len(i) > 1:
                phrases.append(i)
        return (phrases)

    def search_phrases_in_str(self, S):
        phrases = self.search_phrases_in_dir()
        S = str(S)
        out_list = []
        need_skip = 0
        for i in range(len(S)):
            if need_skip != 0:
                need_skip -= 1
                continue
            T = True
            for j in phrases:
                if len(S) - i >= len(j):
                    if S[i:i + len(j)] == j:
                        out_list.append(j)
                        need_skip = len(j) - 1
                        T = False
                        break
            if T:
                out_list.append(S[i])

        return out_list

    def set_to_lower(self, S):
        out_list = []
        is_global_upper = False
        for i in S:
            if i.isupper() and i.isalpha():
                if not is_global_upper:
                    if len(out_list) > 2 and out_list[-2] == self.to_upper:
                        out_list[-2] = self.to_upper_global
                        is_global_upper = True
                    else:
                        out_list.append(self.to_upper)
                out_list.append(i.lower())
            elif i.islower() and i.isalpha():
                if is_global_upper:
                    if len(out_list) > 2 and out_list[-2] == self.to_lower:
                        out_list[-2] = self.to_lower_global
                        is_global_upper = False
                    else:
                        out_list.append(self.to_lower)
                out_list.append(i)
            else:
                out_list.append(i)

        return out_list

    def get_code_str(self, S):
        symbols_list = self.search_phrases_in_str(S)
        symbols_list = self.set_to_lower(symbols_list)
        out_list = []
        for i in symbols_list:
            if i in self.symbols_codes:
                out_list.append(self.symbols_codes[i])
            else:
                out_list = None
                break
        return out_list
