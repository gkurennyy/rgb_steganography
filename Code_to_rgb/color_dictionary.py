class corolDictionary:
	def __init__(self):
		self.colors_to_rgb = {
			'Re': '#f00',
			'Or': '#f80',
			'Ye': '#ff0',
			'Gr': '#0f0',
			'Lb': '#0ff',
			'Bl': '#00f',
			'Pu': '#80f',
			'Pi': '#f0f',
			'Wh': '#fff',
			'BL': '#000'
		}

	def getRgb(self, color):
		return self.colors_to_rgb[color]

	def get_coords_white(self, nX, nY):
		white_arr = []

		white_arr.extend([[1, 0], [0, 1], [nX-1, 0], [nX-2, 0], [nX-1, 1], [0, nY-1], [1, nY-1], [0, nY-2], [nX-1, nY-1]])

		key_x = [0, nX//2 - 1, nX//2]
		if nX%2 == 1:
			key_x.append(nX//2+1)


		key_y = [0, nY//2 - 1, nY//2]
		if nY%2 == 1:
			key_y.append(nY//2+1)

		for i in key_x:
			for j in key_y:
				white_arr.append([i, j])

		return white_arr

	def format_arr(self):
		A = [
			-1, -1, 32, -1, -1, 33, -1, -1,
			-1, 25, 19, 11, 12, 20, 26, -1,
			35, 24, 10,  0,  1,  8, 21, 37,
			-1, 18,  7, -1, -1,  2, 13, 28,
			-1, 17,  6, -1, -1,  3, 14, 29,
			34, 23,  9,  5,  4, 38, 39, 41,
			-1, 27, 22, 16, 15, 40, 43, 44,
			-1, -1, 36, 31, 30, 42, 45, -1
		]

		B = []
		for i in range(max(A)):
			B.append(A.index(i))

		return B