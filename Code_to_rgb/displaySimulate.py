from tkinter import Tk, Canvas, Frame, BOTH
import color_dictionary


class MainFrame(Frame):
	def __init__(self, nX, nY, string=None):
		super().__init__()

		self.nX = nX if nX > 0 else 1
		self.nY = nY if nY > 0 else 1

		self.rOval = 25  # радиус окр
		self.dOval = 10  # расстояние между окр

		self.color_arr = []
		self.dots_arr = []
		self.initDots(string)

	def initCoords(self, center_x, center_y):
		startX = center_x - ((self.nX - 1) * 2 * self.rOval + (self.nX - 1) * self.dOval)
		startY = center_y - ((self.nY - 1) * 2 * self.rOval + (self.nY - 1) * self.dOval)

		d = self.rOval * 2 + self.dOval

		startX = d if startX < d else startX
		startY = d if startY < d else startY

		for i in range(self.nY):
			for j in range(self.nX):
				self.dots_arr.append([
					startX + j * d - self.rOval,
					startY + i * d - self.rOval,
					startX + j * d + self.rOval,
					startY + i * d + self.rOval,
				])

	def initColor(self, string=None):
		dict = color_dictionary.corolDictionary()
		string = string.split(' ') if not string == None else ['Re']*(self.nX * self.nY)
		if len(string) > self.nX * self.nY:
			string = string[:self.nX * self.nY]

		for i in range(len(string)):
			self.color_arr.append(dict.getRgb(string[i]))

		if len(string) < self.nX * self.nY:
			for i in range(len(string), self.nX * self.nY):
				self.color_arr.append('#000')

	def initDots(self, string):
		self.pack(fill=BOTH, expand=1)

		self.initCoords(1000, 600)
		self.initColor(string)

		canvas = Canvas(self)
		canvas.create_rectangle(0, 0, 2000, 1000, fill="#000")
		for i in range(len(self.dots_arr)):
			canvas.create_oval(self.dots_arr[i], fill=self.color_arr[i])

		canvas.pack(fill=BOTH, expand=1)


def main(string=None):
	root = Tk()
	dict_this = color_dictionary.corolDictionary()
	print(dict_this.get_coords_white(8, 8))
	frame = MainFrame(8, 8, string)

	root.mainloop()


if __name__ == "__main__":
	main()
