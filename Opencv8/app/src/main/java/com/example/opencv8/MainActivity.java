package com.example.opencv8;

import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    CameraBridgeViewBase cameraBridgeViewBase;
    BaseLoaderCallback baseLoaderCallback;
    Switch switcher;
    boolean last_frame_is_ready = true;
    boolean first_flame_is_ready = false;
    boolean switch_enabled = false;
    int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cameraBridgeViewBase = (JavaCameraView)findViewById(R.id.CameraView);
        cameraBridgeViewBase.setVisibility(SurfaceView.VISIBLE);
        cameraBridgeViewBase.setCvCameraViewListener(this);

        switcher = (Switch) findViewById(R.id.switch1);

        baseLoaderCallback = new BaseLoaderCallback(this) {
            @Override
            public void onManagerConnected(int status) {
                super.onManagerConnected(status);
                switch (status) {
                    case BaseLoaderCallback.SUCCESS:
                        cameraBridgeViewBase.enableView();
                        break;
                    default:
                        super.onManagerConnected(status);
                        break;
                }
            }
        };

        switcher = (Switch) findViewById(R.id.switch1);

        switcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)  {
                    switch_enabled = true;
                } else {
                    switch_enabled = false;
                }
            }
        });

    }

    Object getCores(ArrayList<Integer> points, int Len) {
        int [][] cores = new int[4][2];
        int xMin = 10000, xMax = 0, yMin = 10000, yMax = 0;

        for (int i = 0; i < Len*2; i+=2) {
            if (points.get(i) < xMin) {
                cores[0] = new int[]{points.get(i), points.get(i + 1)};
                xMin = points.get(i);
            }

            if (points.get(i+1) < yMin) {
                cores[1] = new int[]{points.get(i), points.get(i + 1)};
                yMin = points.get(i+1);
            }

            if (points.get(i) > xMax) {
                cores[2] = new int[]{points.get(i), points.get(i + 1)};
                xMax = points.get(i);
            }

            if (points.get(i+1) > yMax) {
                cores[3] = new int[]{points.get(i), points.get(i + 1)};
                yMax = points.get(i+1);
            }
        }

        return cores;
    }

    Object getDots(int[][] cores, int parts) {
        int[][] Dots = new int[parts*parts][2];

        double xS1 = cores[0][0];
        double dx1 = (cores[1][0] - xS1) / (parts-1.0);

        double xS2 = cores[3][0];
        double dx2 = (cores[2][0] - xS2) / (parts-1.0);

        double yS1 = cores[0][1];
        double dy1 = (cores[1][1] - yS1) / (parts-1.0);

        double yS2 = cores[3][1];
        double dy2 = (cores[2][1] - yS2) / (parts-1.0);

        for (int i = 0; i < parts; i++) {
            double x1 = xS1 + dx1*i;
            double y1 = yS1 + dy1*i;
            double x2 = xS2 + dx2*i;
            double y2 = yS2 + dy2*i;

            double dx = (x2 - x1) / (parts-1);
            double dy = (y2 - y1) / (parts-1);
            for (int j = 0; j < parts; j++) {
                Dots[i*parts+j] = new int[]{(int) (x1+dx*j), (int) (y1+dy*j)};
            }

        }

        return Dots;
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat frame = inputFrame.rgba();
        //Mat M = Imgproc.getRotationMatrix2D(new Point(frame.width() / 2, frame.height()/2), 270, 1);

        //Imgproc.warpAffine(frame, frame, M, new Size(frame.width(), frame.height()), Imgproc.INTER_LINEAR, Core.BORDER_TRANSPARENT);

        //M.release();
        if (switch_enabled) {
                    Imgproc.cvtColor(frame, frame, Imgproc.COLOR_RGBA2RGB);
                    /*
                    for (int i = 0; i < frame.rows(); i++)
                        for (int j = 0; j < frame.cols(); j++) {
                            double[] arr = frame.get(i, j);
                            if ((arr[0] >= arr[1] - 10 ||
                                    arr[2] >= arr[1] - 10 ||
                                    arr[1] <= 64)) {
                                for (int k = 0; k < arr.length; k++)
                                    arr[k] = 0;

                                frame.put(i, j, arr);
                            }
                        } */

                    /*
                    Mat img2 = new Mat();
                    Mat frame2 = new Mat();
                    Imgproc.GaussianBlur(frame, img2, new Size(9, 9), 0);
                    Core.addWeighted(frame, 1.5, img2, -0.5, 0, frame2);
                    img2.release();
                    */
                    Mat hsv = new Mat();
                    Imgproc.cvtColor(frame, hsv, Imgproc.COLOR_RGB2HSV);
                    int col = frame.cols() / 2, row = frame.rows() / 2;
                    Log.i("frame_color", String.valueOf(hsv.get(row, col)[0]) + " " + String.valueOf(hsv.get(row, col)[1]) + " " + String.valueOf(hsv.get(row, col)[2]));

                    //Mat green_filter = Green_filter_hsv(hsv, frame);
                    /*Mat blue_filter*/ //frame =  Blue_filter_hsv(hsv, frame);
                    /*
                    Mat r1_mask = new Mat();
                    Mat r2_mask = new Mat();
                    Mat g_mask = new Mat();
                    Mat b_mask = new Mat();
                    Mat mask1 = new Mat();

                    Core.inRange(hsv, new Scalar(0, 110, 50), new Scalar(10, 255, 255), r1_mask);
                    Core.inRange(hsv, new Scalar(245, 110, 50), new Scalar(255, 255, 255), r2_mask);
                    Core.inRange(hsv, new Scalar(40, 110, 50), new Scalar(70, 255, 255), g_mask);
                    Core.inRange(hsv, new Scalar(90, 90, 50),  new Scalar(130, 255, 255), b_mask);
                    */
                    Mat empty = new Mat(frame.rows(), frame.cols(), frame.type(), new Scalar(0, 0, 0));

                    Mat white_mask = new Mat();
                    Core.inRange(hsv, new Scalar(0, 0, 235), new Scalar(255, 20, 255), white_mask);

                    hsv.release();
                    //Imgproc.cvtColor(frame, hsv, Imgproc.COLOR_RGB2GRAY);
                    Mat circles = new Mat();
                    //Imgproc.HoughCircles(white_mask, circles, Imgproc.CV_HOUGH_GRADIENT, 30, 70, 130, 250 , 20, 80);
                    Core.add(empty, frame, hsv, white_mask);
                    Imgproc.GaussianBlur(frame, frame, new Size(3, 3), 2);

                    Log.i("Circles: ", String.valueOf(circles.rows()));
                    /*
                    for (int i = 0, r = circles.rows(); i < r; i++) {
                        for (int j = 0, c = circles.cols(); j < c; j++) {
                            double[] circle = circles.get(i, j);
                            Imgproc.circle(frame, new Point(circle[0], circle[1]), (int) circle[2], new Scalar(0, 255, 0), 20);
                        }
                    } /**/

                    circles.release();
                    //Core.add(empty, frame, hsv, white_mask);

                    //Core.add(g_mask, b_mask, mask1);
                    //Core.add(empty, frame, hsv, r1_mask);
                    //Core.add(hsv, frame, hsv, r2_mask);
                    //Core.add(hsv, frame, hsv, g_mask);
                    //Core.add(hsv, frame, hsv, b_mask);

                    //frame = hsv;

                    //frame.release();
                    //Core.add(green_filter, blue_filter, frame);
                    /*Mat h = new Mat();
                    Mat frame1 = new Mat();
                    Imgproc.cvtColor(frame, frame1, Imgproc.COLOR_RGB2HSV);

                    //Log.i("first pixel", String.valueOf(hsv.get(row, col)[0]) + " " + String.valueOf(hsv.get(row, col)[1]) + " " + String.valueOf(hsv.get(row, col)[2]));
                    Core.inRange(hsv, new Scalar(40, 110, 50), new Scalar(80, 255, 255), frame1);
                    hsv.release();
                    h.release();
                    frame1.convertTo(frame1, -1, 1.0/255);

                    Imgproc.cvtColor(frame1, frame1, Imgproc.COLOR_GRAY2RGB);

                    Core.multiply(frame, frame1, frame);
                    /**/


                    //Imgproc.GaussianBlur(hsv, frame, new Size(3, 3), 2);

                    ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();

                    Imgproc.findContours(white_mask, contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
                    //Imgproc.drawContours(frame, contours, -1, new Scalar(255, 255, 255));
                    //Mat edges = new Mat();

                    //Imgproc.Canny(white_mask, edges, 127, 256);


                    //Log.i("Contours", String.valueOf(contours.size()));

                    ArrayList<Integer> edjesMax = new ArrayList<>();
                    int counts = 0;

                    for (int i = 0, j = contours.size(); i < j; i++) {
                        //System.out.println(Imgproc.contourArea(contours.get(i)));
                        Rect r = Imgproc.boundingRect(contours.get(i));
                        //System.out.println("boundingRect = " + r);
                        //double len = Imgproc.arcLength(new MatOfPoint2f(contours.get(i).toArray()), true);
                        //System.out.println("arcLength = " + len);
                        if (r.width > 30 && r.height > 30 && r.width < 300 && r.height < 300) {
                            int x1 = (r.x * 2 + r.width - 1) / 2;
                            int y1 = (r.y * 2 + r.height - 1) / 2;

                            edjesMax.add(x1);
                            edjesMax.add(y1);
                            counts++;
                            Imgproc.circle(frame, new Point(x1, y1), 30, new Scalar(255, 0, 0), 6);
                            //Imgproc.rectangle(frame, new Point(r.x, r.y), new Point(r.x + r.width - 1, r.y + r.height - 1), new Scalar(0, 255, 0));
                        }

                    }
                    int[][] cores = (int[][]) getCores(edjesMax, counts);
                    int[][] dots = (int[][]) getDots(cores, 8);
                    for (int i = 0; i < cores.length; i++) {
                        //Log.i("Dots: ", String.valueOf(cores[i][0]) + ' ' + cores[i][1]);
                        Imgproc.circle(frame, new Point(cores[i][0], cores[i][1]), 40, new Scalar(0, 255, 0), 8);
                        //Imgproc.circle(frame, new Point(edjesMax.get(i), edjesMax.get(i)), 40, new Scalar(0, 255, 0), 8);
                    }

                    for (int i = 0; i < dots.length; i++) {
                        Imgproc.circle(frame, new Point(dots[i][0], dots[i][1]), 20, new Scalar(255, 255, 255), 8);
                    }
                    Imgproc.circle(frame, new Point(col, row), 10, new Scalar(255, 0, 0));
                    //green_filter.release();
                    //blue_filter.release();

                    /*
                    r1_mask.release();
                    r2_mask.release();
                    g_mask.release();
                    b_mask.release();
                    mask1.release();
                    */
                    empty.release();
                    white_mask.release();

                    hsv.release();
        }

        return frame;
    }

    @Override
    public void onCameraViewStarted(int width, int height) {

    }

    @Override
    public void onCameraViewStopped() {

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!OpenCVLoader.initDebug()) {
            Toast.makeText(getApplicationContext(), "There is a problem", Toast.LENGTH_LONG).show();
        }
        else {
            cameraBridgeViewBase.enableView();
            cameraBridgeViewBase.enableFpsMeter();
            baseLoaderCallback.onManagerConnected(baseLoaderCallback.SUCCESS);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (cameraBridgeViewBase != null) {
            cameraBridgeViewBase.disableView();
            cameraBridgeViewBase.disableFpsMeter();

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (cameraBridgeViewBase != null) {
            cameraBridgeViewBase.disableView();
            cameraBridgeViewBase.disableFpsMeter();
        }
    }

    /*public static double[] RGBtoHSV(double[double r, double g, double b]){

        double h, s, v;

        double min, max, delta;

        min = Math.min(Math.min(r, g), b);
        max = Math.max(Math.max(r, g), b);

        // V
        v = max;

        delta = max - min;

        // S
        if( max != 0 )
            s = delta / max;
        else {
            s = 0;
            h = -1;
            return new double[]{h,s,v};
        }

        // H
        if( r == max )
            h = ( g - b ) / delta; // between yellow & magenta
        else if( g == max )
            h = 2 + ( b - r ) / delta; // between cyan & yellow
        else
            h = 4 + ( r - g ) / delta; // between magenta & cyan

        h *= 60;    // degrees

        if( h < 0 )
            h += 360;

        return new double[]{h,s,v};
    }*/
}